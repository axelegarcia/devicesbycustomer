import customer_info as ci
import json
import csv
from datetime import datetime
import cProfile
import pstats

# List of column headers
csv_column_headers = [
    "Customer Name",
    "Group Name",
    "Platform",
    "imei",
    "Main",
    "VCM",
    "Power",
    "Bluetooth",
    "settings",
    "checkinDate",
]
# List is used to remove unwanted keys in the dictionaries returned
# by API calls
keys_to_remove = ["iccid", "imsi", "msisdn", "lat", "lng", "locDate"]
list_valid_pltfrms = [
    "80",
    "90",
    "95",
    "0500",
    "0502",
    "0504",
    "1127",
    "1500",
    "2000",
    "2010",
    "2050",
    "2070",
    "2127",
    "2128",
    "2500",
    "3000",
    "3001",
    "3125",
    "3127",
    "4127",
    "6000",
    "6004",
    "6005",
    "6030",
    "7000",
]

# TODO Comment code


def main():
    print("List of valid platforms: ", end="")
    print(*list_valid_pltfrms, sep=", ")
    pltfrm_in = input(
        "Please enter the platform name (i.e 2050, 2500, 90, 95): "
    )
    while pltfrm_in not in list_valid_pltfrms:
        pltfrm_in = input(
            "Please enter a valid platform name: ",
        )
    file_path = input(
        "Please enter a name for the output file or a path such as /path/<name>.csv: "
    )
    if ".csv" not in file_path:
        file_path = file_path + ".csv"
    # Uncomment this code to see function performance
    # profile = cProfile.Profile()
    # profile.runcall(process_data,pltfrm_in, file_path)
    # ps = pstats.Stats(profile).sort_stats('tottime')
    # ps.print_stats()
    # Make function call below a comment if above is uncommented
    process_data(pltfrm_in, file_path)


def process_data(platform_name="", file_path=""):
    """Processes the data received from API and writes to csv file.

    Args:
        platform_name (str, optional): Platform number user wants data for. Defaults to ''.
        file_path (str, optional): Path or filename user wants to write. Defaults to ''.
    """
    a = ci.info(
        "prod", "jorma.seppanen@positioninguniversal.com", "P0s1ti0Ning1234!$$"
    )
    # Write column headers to CSV file
    with open(file_path, "w", encoding="utf8", newline="") as output_file:
        fc = csv.DictWriter(output_file, fieldnames=csv_column_headers)
        fc.writeheader()
    # Init empty list where groups of matching platform will go
    matching_platform_list = []
    try:
        with open("customerDictionary.json", encoding="utf8") as json_file:
            data = json.load(json_file)

            # TODO Remove nested for loops
            for d in data["customers"]:
                # For each customer, grab all groups
                l = a.get_all_groups(d["customerId"])
                for k in l:
                    # Look for groups with platform equal to one user needs
                    if k["platform"] == platform_name:
                        # Add customer name and customer id to group dictionaries
                        k["customerName"] = d["customerName"]
                        k["customerId"] = d["customerId"]
                        # Append group dictionaries to list
                        matching_platform_list.append(k)
        for x in matching_platform_list:
            # Get devices from each group in our list of groups
            devices = a.get_devices_by_group(x["id"], x["customerId"])
            # Filter out devices which have no firmwares on them.
            filtered_data = [x for x in devices if x["firmwares"]]
            for xx in filtered_data:
                # Remove unwanted keys from each device dictionary
                [xx.pop(key) for key in keys_to_remove]
                # Add Customer Name key to each device's dictionary
                xx["Customer Name"] = x["customerName"]
                # Format timestamp from UNIX to human readable
                xx["checkinDate"] = datetime.utcfromtimestamp(
                    xx["checkinDate"]
                ).strftime("%m-%d-%y %H:%M:%S")
                # Add Group Name and Platform keys to each device's dictionary
                xx["Group Name"] = x["name"]
                xx["Platform"] = x["platform"]
                # Flatten firmwares dictionary that is found as value of firmwares key
                # in each device's dictionary
                for yy in xx["firmwares"]:
                    # Add key to device's dictionary, where key is the type of
                    # firmware and value is the firmware version
                    xx[str(yy["type"])] = yy["tag"]
                # Delete firmwares key in each device's dictionary as we have flattened it
                del xx["firmwares"]
            # Append to CSV file we wrote to previously
            with open(
                file_path, "a", encoding="utf8", newline=""
            ) as output_file:
                fc = csv.DictWriter(output_file, fieldnames=csv_column_headers)
                fc.writerows(filtered_data)
    except Exception as e:
        print(str(e))


if __name__ == "__main__":
    main()
