import hashlib
import hmac
import time
import requests

class info:
    DMAN_API_key = (
        "6195f21585c598c740928f29ed4ebd025fffe15772342f24fb21c4f6a14b9f6f"
    )
    passhash = ""
    dman_groups = None

    api_msg = {
        "action": "",
        "customerId": 0,
        "login": "",
        "time": 0,
        "hmac": "",
    }
    api_msg_imei = {
        "action": "",
        "imei": "",
        "login": "",
        "time": 0,
        "hmac": "",
    }
    api_msg_devices_by_group = {
        "action": "",
        "customerId": 31,
        "groupId": 0,
        "login": "",
        "time": 0,
        "hmac": "",
    }
    api_msg_group = {
        "action": "",
        "imeis": "",
        "data": "",
        "customerId": 0,
        "groupId": 0,
        "login": "",
        "time": 0,
        "hmac": "",
    }

    def __init__(self, server, login, password):
        byte_key = bytes(self.DMAN_API_key, "UTF-8")
        self.passhash = hmac.new(
            byte_key, password.encode(), hashlib.sha256
        ).hexdigest()
        self.api_msg["login"] = login
        self.api_msg_imei["login"] = login
        self.api_msg_group["login"] = login
        self.api_msg_devices_by_group["login"] = login

        if server == "test":
            self.URL = "https://test-api.dmanservice.com/v4"
        else:
            self.URL = "https://api.dmanservice.com/v4"
        self.passhash_byte_key = bytes(self.passhash, "UTF-8")
        self.get_all_groups(0)

    def get_all_groups(self, customerID):
        self.api_msg["action"] = "getAllGroups"
        try:
            self.dman_groups = self.send_api_message("", None, customerID)
            return self.dman_groups
        except Exception as e:
            raise Exception(str(e))

    def get_device_by_imei(self, imei):
        self.api_msg_imei["action"] = "getDevicesByImeis"
        try:
            ret = self.send_api_message(imei)
            return ret
        except Exception as e:
            raise Exception(str(e))

    def get_devices_by_group(self, group_Id, customerID):
        self.api_msg_devices_by_group["action"] = "getDevicesInGroup"
        try:
            ret = self.send_api_message("", group_Id, customerID)
            return ret
        except Exception as e:
            raise Exception(str(e))

    def send_api_message(self, imei="", group_id=None, customerID=None):
        tm = int(time.time())
        self.api_msg["time"] = tm
        self.api_msg_imei["time"] = tm
        self.api_msg_group["time"] = tm
        self.api_msg_devices_by_group["time"] = tm

        if group_id is not None:
            if self.api_msg_group["action"] != "":
                action_str = (
                    "action="
                    + self.api_msg_group["action"]
                    + "&imeis="
                    + imei
                    + "&data="
                    + imei
                    + "&customerId=0"
                    + "&groupId="
                    + str(group_id)
                    + "&login="
                    + self.api_msg["login"]
                    + "&time="
                    + str(tm)
                )
                self.api_msg_group["imeis"] = imei
                self.api_msg_group["data"] = imei
                self.api_msg_group["groupId"] = group_id
                self.api_msg_group["hmac"] = hmac.new(
                    self.passhash_byte_key, action_str.encode(), hashlib.sha256
                ).hexdigest()
                resp = requests.post(self.URL, json=self.api_msg_group)
            else:
                self.api_msg_devices_by_group["customerId"] = customerID
                action_str = (
                    "action="
                    + self.api_msg_devices_by_group["action"]
                    + "&customerId="
                    + str(customerID)
                    + "&groupId="
                    + str(group_id)
                    + "&login="
                    + self.api_msg["login"]
                    + "&time="
                    + str(tm)
                )
                self.api_msg_devices_by_group["groupId"] = group_id
                self.api_msg_devices_by_group["hmac"] = hmac.new(
                    self.passhash_byte_key, action_str.encode(), hashlib.sha256
                ).hexdigest()
                resp = requests.post(
                    self.URL, json=self.api_msg_devices_by_group
                )
        elif imei != "":
            action_str = (
                "action="
                + self.api_msg_imei["action"]
                + "&imei="
                + imei
                + "&login="
                + self.api_msg["login"]
                + "&time="
                + str(tm)
            )
            self.api_msg_imei["imei"] = imei
            self.api_msg_imei["hmac"] = hmac.new(
                self.passhash_byte_key, action_str.encode(), hashlib.sha256
            ).hexdigest()
            resp = requests.post(self.URL, json=self.api_msg_imei)
        else:
            self.api_msg["customerId"] = customerID
            action_str = (
                "action="
                + self.api_msg["action"]
                + "&customerId="
                + str(self.api_msg["customerId"])
                + "&login="
                + self.api_msg["login"]
                + "&time="
                + str(tm)
            )
            self.api_msg["hmac"] = hmac.new(
                self.passhash_byte_key, action_str.encode(), hashlib.sha256
            ).hexdigest()
            resp = requests.post(self.URL, json=self.api_msg)

        dman_resp = eval(resp.text)
        if dman_resp["status"] == 200:
            return dman_resp["data"]
        else:
            raise Exception(
                "Error: " + str(dman_resp["status"]) + " " + dman_resp["msg"]
            )
